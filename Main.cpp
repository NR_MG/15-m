#include <iostream>

int N = 89;

void EvenOdd(int a)
{
	switch (a)
	{
		case 1:
			for (int i = 0; i <= N; i++)
			{
				if (i % 2 == 1)
				{
					std::cout << i << "\n";
				}
			}
			break;
		case 2:
			for (int i = 0; i <= N; i++)
			{
				if (i % 2 == 0)
				{
					std::cout << i << "\n";
				}
			}
			break;
		default:
			std::cout << "Enter 1 or 2\n";
			break;
	}
}

int main()
{
	for (int i = 0; i <= N; i++)
	{
		if (i % 2 == 0)
		{
			std::cout << i << "\n";
		}
	}
	EvenOdd(5);

	return 0;
}
